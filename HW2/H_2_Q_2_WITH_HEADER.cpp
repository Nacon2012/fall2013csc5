/*
* Name: Benjamin Cornejo
* Student ID: 2203277
* Date: 16 September 2013
* HW: 2
* Problem: 2
* I certify This is my own work and code
*/

#include <iostream>

using namespace std;

int main()
{
    string name1 = "Calvin";
    string name2 = "Bill";
    
    cout << "Hello my name is " << name1 << endl;
    cout << "Hi There " << name1 << " ! My Name is " << name2 << endl;
    cout << "Nice to meet you " << name2 << " .\n"
         << "I am wondering if you're available for what?\n";
    cout << "What? Bye" << name1 << "!\n";
    cout << "Uh... sure, bye " << name2 << " !\n\n";
    cout << name1 << name1 << name1 << name1 << name1 << endl;
    cout << name2 << name2 << name2 << name2 << name2 << endl;
    system("Pause");
    return 0;
}
    
