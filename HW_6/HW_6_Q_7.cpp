/*
* Name: Benjamin Cornejo
* Student ID: 2203277
* Date: October 22 2013
* HW: 6
* Problem: 7
* I certify this is my own work and code
*/


#include <iostream>

using namespace std;

int meters;

double centimeters;

void convertimp_to_met(double x, double y);

void output(int x, double y);

void input(int&feet, int&inches);

int main()
{
    
    int feet = 1, inches = 1;
    do
    {
        cout << "Enter, '0' for feet and '0' to inches to stop playing \n";
        input(feet, inches);
        
        if(feet == 0 && inches == 0)
        {
            cout << "Thanks for playing \n";
        }
        
        else
        {
            convertimp_to_met(feet, inches);
            output(meters, centimeters);
        }
    }
    while(feet != 0 || inches!=0);
    
    cout << " \n";
    system("pause");
    return 0;
}

void convertimp_to_met(double x, double y)
{
     double totali;
     totali = x+(y/12); //Totali is in feet
     
     // 1 foot = .3048 meters;
     double metric_i;
     metric_i = totali*.3048*100; // this is in Centimeters;
     
     
     meters = static_cast<int>(metric_i)/100;
     
     double leftover_centimeters;
     leftover_centimeters = metric_i - meters*100;
     centimeters = leftover_centimeters;
     
}

void output(int x, double y)
{
     cout << "Per Metric System, your height is: " << x << " meters and "
     << y << " centimeters \n" ;
}

void input(int&feet, int&inches)
{
    cout << "User, How tall are you? \n";
    cout << "Enter the greatest Integer equal to or less than your Height " <<
    "in feet \n";
    cin >> feet;
    cout << " \n";
    cout << "Now, enter, the remainder in inches \n";
    cin >> inches;
}
