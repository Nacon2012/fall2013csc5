#include <iostream>
#include <iomanip>
using namespace std;
int main ()
{
    float J_Q_1, J_Q_2, J_Q_3, J_Q_4, A_Q_1, A_Q_2, A_Q_3, A_Q_4, T_Q_1, T_Q_2, T_Q_3, T_Q_4;
    cout << "Dear Busy Professor, This program will allow you to Average the Scores for each Quiz \n";
    cout << "Enter John's Scores" << " Quiz 1 \n";
    cin >> J_Q_1;
    cout << "Quiz 2 \n";
    cin >> J_Q_2;
    cout << "Quiz 3 \n";
    cin >> J_Q_3;
    cout << "Quiz 4 \n";
    cin >> J_Q_4;
    cout << "Enter Mary's Scores" << " Quiz 1 \n";
    cin >> A_Q_1;
    cout << "Quiz 2 \n";
    cin >> A_Q_2;
    cout << "Quiz 3 \n";
    cin >> A_Q_3;
    cout << "Quiz 4 \n";
    cin >> A_Q_4;
    cout << "Enter Matt's Scores" << " Quiz 1 \n";
    cin >> T_Q_1;
    cout << "Quiz 2 \n";
    cin >> T_Q_2;
    cout << "Quiz 3 \n";
    cin >> T_Q_3;
    cout << "Quiz 4 \n";
    cin >> T_Q_4;
    float sum_Q_1, sum_Q_2, sum_Q_3, sum_Q_4;
    float ave_Q_1, ave_Q_2, ave_Q_3, ave_Q_4;
    sum_Q_1 = J_Q_1 + A_Q_1 + T_Q_1;
    sum_Q_2 = J_Q_2 + A_Q_2 + T_Q_2;
    sum_Q_3 = J_Q_3 + A_Q_3 + T_Q_3;
    sum_Q_4 = J_Q_4 + A_Q_4 + T_Q_4;
    ave_Q_1 = sum_Q_1 / 3;
    ave_Q_2 = sum_Q_2 / 3;
    ave_Q_3 = sum_Q_3 / 3;
    ave_Q_4 = sum_Q_4 / 3;
    cout << setw(12) << left << "name" << setw(9) << right << "Quiz 1" << setw(9) << right << "Quiz 2" << setw(9) << right << "Quiz 3" << setw(9) << right << "Quiz 4" << endl;
    cout << setw(12) << left << "----" << setw(9) << right << "------" << setw(9) << right << "------" << setw(9) << right << "------" << setw(9) << right << "------" << endl;
    cout << setw(12) << left << "John" << setw(7) << right << J_Q_1 << "  " << setw(7) << right << J_Q_2 << "  " << setw(7) << right << J_Q_3 << "  " << setw(7) << right << J_Q_4 << "  " << endl;
    cout << setw(12) << left << "Mary" << setw(7) << right << A_Q_1 << "  " << setw(7) << right << A_Q_2 << "  " << setw(7) << right << A_Q_3 << "  " << setw(7) << right << A_Q_4 << "  " << endl;
    cout << setw(12) << left << "Matt" << setw(7) << right << T_Q_1 << "  " << setw(7) << right << T_Q_2 << "  " << setw(7) << right << T_Q_3 << "  " << setw(7) << right << T_Q_4 << "  " << endl;
    cout << " \n";
    cout << setw(12) << left << "Average" << setw(7) << right << fixed << setprecision(2) << ave_Q_1 << "  " << setw(7) << right << ave_Q_2 << "  " << setw(7) << right << ave_Q_3 << "  " << setw(7) << right << ave_Q_4 << "  " << endl;
    cout << " \n";
    
    
    
    system("Pause");
    return 0;
}
