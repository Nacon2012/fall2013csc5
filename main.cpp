#include <iostream>
// iostream is the library, and it allows input and output
using namespace std;
//std is the location of the Library
int main ( )
{ 
    //Hello World Program
    cout << "Hello World" << endl;
    // "Hello World" is a String Literal, and this line is a comment
    // "endl" means endline
    // "cout" initiates an output
    // "endl" can be replaced with "\n"
    
    return 0;
}