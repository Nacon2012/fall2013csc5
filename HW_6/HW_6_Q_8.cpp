/*
* Name: Benjamin Cornejo
* Student ID: 2203277
* Date: October 22 2013
* HW: 6
* Problem: 8
* I certify this is my own work and code
*/


#include <iostream>
using namespace std;

void output_quarters(int&cents)
{
    int quarters = cents/25;
    
    cents = cents - quarters*25;
    
    cout << quarters << " Quarter(s) ";
}

void output_dimes(int&cents_after_quarters_subtracted)
{
    int dimes = cents_after_quarters_subtracted/10;
    
    cents_after_quarters_subtracted -= dimes*10;
    
    cout << dimes << " Dime(s) and ";
}

void output_pennies(int&cents_after_dimes_subtracted)
{
    int pennies = cents_after_dimes_subtracted;
    
    cout << pennies << " Pennies \n";
}
    

int main()
{
    int change;
    
    cout << "User, enter your life savings, assume cents, no decimals \n";
    cout << "Enter, '0' to not play \n";
    cin >> change;
    while(change !=0)
    {
        
        cout << " \n";
        output_quarters(change);
        output_dimes(change);
        output_pennies(change);
        
        cout << "User, enter your life savings, assume cents, no decimals \n";
        cout << "Enter '0' to stop playing \n";
        cin >> change;
        
    }
    
    
    
    
    cout << " \n";
    system("pause");
    return 0;
}
    
