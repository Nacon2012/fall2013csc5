/*
* Name: Benjamin Cornejo
* Student ID: 2203277
* Date: October 22 2013
* HW: 6
* Problem: 4
* I certify this is my own work and code
*/
#include <iostream>
#include <iomanip>
using namespace std;
//Comments for Question 1;

void create_line(int x_intpt, int y_intpt)// Same Name as other function, but
//Receives different parameters -- This one receives Axis intercepts
//as integers, and computes the Equation of the line based that, but only uses
//integers, void so, it returns nothing, but it does have some output.

//Does not validate user's input but works around it, because with a value of
//zero for either intercepts, the equation of the line is undeterminable by
//intercepts alone for that case.

//however, does put that into account, and prompts the user.
{
    int slope;
    if(x_intpt == 0 || y_intpt == 0)
    {
        cout << "Slope and line equation undeterminable by intercepts \n";
    }
    else
    {
        slope = -1*y_intpt/x_intpt;
        int m = slope;
        int b = y_intpt;
        cout << "Equation of the line is: y = " << m << "*x + " << b;
    }
}

void create_line(double x_intpt, double y_intpt)
/*Receives doubles, as opposed to ints, the purpose is to show a more accurate
representation, of the equation of the line because operations are not limited
to doubles.

Does not validate user's input but works around it, because with a value of
zero for either intercepts, the equation of the line is undeterminable by
intercepts alone for that case.

however, does put that into account, and prompts the user.

*/
{
    double slope;
    
    if(x_intpt*1 == 0 || y_intpt*1 == 0)
    {
        cout << "Slope and line equation undeterminable by intercepts \n";
    }
    else
    {
        slope = -1*y_intpt/x_intpt;
        double m = slope;
        double b = y_intpt;
    
        cout << "Equation of the line is: y = ";
        cout << fixed << setprecision(2) << m << "*x + " << b;
    }

    
}


//Comments for Questions 2;

void icecream_rations(double x, int y)
/*
outputs to the manager of 
receives Amount of pounds of ice cream as a double in from user;
and receives the amount of customers attending;

if there are zero customers, the the function is set to output to the user
that the amount is undefined.

void, so, returns no Value, except for output.

*/
{
    double ration;
    switch(y)
    {
        case 0:
            {
                cout << x << "/" << "y is undefined \n";
                cout << "You have unlocked the portal to Earth's Core \n";
            }
        break;
        default:       
            ration = (x/y);
            cout << "The amount of pounds of ice cream per customer is: ";
            cout << ration;
        break;
    }
}

int main()
{
    cout << "User, which game do you want to play \n";
    cout << "User, do you want to play Ice Cream Man, or Math Teacher \n";
    cout << "Enter, '0' for Ice Cream, '1' for Line Equation \n";
    
    int useri = 0;
    cout << "Enter, '2' to quit \n";
    cin >> useri;
    while(useri!=2)
    {
        switch(useri)
        {
            case 0:
                {
                    cout << "What is the weight of the Ice Cream Available \n";
                    double iceweight;
                    cin >> iceweight;
                    cout << " \n";
                    cout << "How many customers exist? \n";
                    int customers_quantity;
                    cin >> customers_quantity;
                    cout << " \n";
                    icecream_rations(iceweight, customers_quantity);
                    cout << " \n";
                    cout << " \n";
                }
            break;
            case 1:
                {
                    int x, y;
                    double x_1, y_1;
                    cout << "Enter, x-intercept \n";
                    cin >> x;
                    x_1 = static_cast<int>(x);
                    cout << "Enter, y-intercept \n";
                    cin >> y;
                    y_1 = static_cast<int>(y);
    
                    create_line(x, y);
                    cout << " \n";
    
                    create_line(x_1, y_1);
    
                    cout << " \n";
                }
            break;
        }
        cout << "Enter, '0' for Ice Cream, '1' for Line Equation \n";
        cout << "Enter, '2' to quit \n";
        cin >> useri;
    }
    
    cout << " \n";
    
    system("pause");
    return 0;
}
                    
    
    


