#include <iostream>
#include <iomanip>
using namespace std;

double liter = .264179;

double miles_per_gallon(double liter_consumed, double miles_driven)
{
       double milespergallon = miles_driven/(liter_consumed*liter);
       return milespergallon;
}
int main()
{
    cout << "This will calculate miles per gallon, for you \n";
    cout << " \n";
    cout << " \n";
    double x, y;
    cout << "How many liters has your car Consumed? \n";
    cin >> x;
    cout << " \n";
    cout << "How many miles have you traveled during that interval? \n";
    cin >> y;
    double mpg = miles_per_gallon(x, y);
    cout << "MPG is: " << fixed << setprecision(2) << mpg << " \n";
    
    cout << " \n";
    system("pause");
    return 0;
}
