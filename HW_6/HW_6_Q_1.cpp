/*
* Name: Benjamin Cornejo
* Student ID: 2203277
* Date: October 22 2013
* HW: 6
* Problem: 1
* I certify this is my own work and code
*/
#include <iostream>
#include <iomanip>
using namespace std;


void create_line(int x_intpt, int y_intpt)
{
    int slope;
    if(x_intpt == 0 || y_intpt == 0)
    {
        cout << "Slope and line equation undeterminable by intercepts \n";
    }
    else
    {
        slope = -1*y_intpt/x_intpt;
        int m = slope;
        int b = y_intpt;
        cout << "Equation of the line is: y = " << m << "*x + " << b;
    }
}

void create_line(double x_intpt, double y_intpt)
{
    double slope;
    
    if(x_intpt*1 == 0 || y_intpt*1 == 0)
    {
        cout << "Slope and line equation undeterminable by intercepts \n";
    }
    else
    {
        slope = -1*y_intpt/x_intpt;
        double m = slope;
        double b = y_intpt;
    
        cout << "Equation of the line is: y = ";
        cout << fixed << setprecision(2) << m << "*x + " << b;
    }

    
}

int main()
{
    int x, y;
    double x_1, y_1;
    cout << "Enter, x-intercept \n";
    cin >> x;
    x_1 = static_cast<int>(x);
    cout << "Enter, y-intercept \n";
    cin >> y;
    y_1 = static_cast<int>(y);
    
    create_line(x, y);
    cout << " \n";
    
    create_line(x_1, y_1);
    
    cout << " \n";
    
    system("pause");
    
    return 0;
    
}
    
    
