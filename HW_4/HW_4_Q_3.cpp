#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    int number, possible, total = 0;
    float score = 0, collected = 0, percentage;
    cout << "Enter How Many Exercises to input: \n";
    cin >> number;
    for (int i = 0; i < number; i++)
    {
        cout << " \n";
        cout << "Score received for exercise " << i+1 << " \n";
        cin >> score;
        cout << " \n";
        collected += score;
        cout << "Total points possible for exercise " << i+1 << " \n";
        cout << " \n";
        cout << "            ";
        cin >> possible;
        total += possible;
    }
    
    cout << " \n";
    cout << " \n";
    percentage = (collected / total)*100;
    
    cout << "Your total is " << collected << " out of ";
    cout << total << " or " << fixed << setprecision(2) << percentage << "%";
    
    
    cout << " \n";
    system("pause");
    return 0;
}
