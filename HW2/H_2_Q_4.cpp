/*
* Name: Benjamin Cornejo
* Student ID: 2203277
* Date: 16 September 2013
* HW: 2
* Problem: 4
* I Certify this is my own work and code

*/
#include <iomanip>
#include <iostream>
using namespace std;
int main ()
{
    
    cout << "This is Mad Lib, input as asked, and keep each individual input \nto a maximum of 10 characters \n";
    string a_name, b_name, food, number_100_120, adjective, color, animal;
    cout << "Enter Your Professor's Name \n";
    cin >> a_name;
    cout << "Enter your name \n";
    cin >> b_name;
    cout << "Enter a food item \n";
    cin >> food;
    cout << "Enter a number between 100 and 120 \n";
    cin >> number_100_120;
    cout << "Enter an Adjective \n";
    cin >> adjective;
    cout << "Enter a color \n";
    cin >> color;
    cout << "Enter an Animal \n";
    cin >> animal;
    cout << "Dear " << a_name << ", \n";
    cout << "I am sorry that I am unable to turn in my homework at this time. \n";
    cout << "First, I ate a rotten " << food << ", which made me turn \n"; 
    cout << color <<" and extremely ill. I came down with a fever of \n"; 
    cout << number_100_120 << ". Next, my " << adjective << " pet \n" << animal;
    cout << " must have smelled the remains of the " << food << " on my homework \n";
    cout << " because he ate it. I am currently rewriting my homework and hope \n";
    cout << "you will accept it late. \n";
    cout << " \n";


    cout << "Sincerely, \n";
    cout << b_name;

    system("pause");
    return 0;
}
