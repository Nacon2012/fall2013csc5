/*
* Name: Benjamin Cornejo
* Student ID: 2203277
* Date: October 22 2013
* HW: 6
* Problem: 6
* I certify this is my own work and code
*/

#include<iostream>
#include<iomanip>
using namespace std;

double grams;

int kilos;

void convertimperialtometric(double pounds, double ounces);

void output(int kilos, double grams);

void input(double&pounds, double&ounces);

int main()
{
    double pounds = 1, ounces = 1;
    do
    
    {
    cout << "Enter '0' for both upcoming values to stop playing \n";
    input(pounds, ounces);
    if(pounds == 0 && ounces == 0)
    {
        cout << "Thanks for playing \n";
    }
    
    else
    {
        
        convertimperialtometric(pounds, ounces);
    
        cout << " \n";
        output(kilos, grams);
        cout << " \n";
    }
    }
    while(pounds !=0 || ounces !=0);
    
    system("pause");
    return 0;
}

void convertimperialtometric(double pounds, double ounces)
{
     double totali = pounds+(ounces/16); // There are 16 ounces per pound;
     double in_metric;
     in_metric = totali*(1/2.2046)*1000;
     double in_metric_int;
     in_metric_int = in_metric;
     kilos = static_cast<int>(in_metric_int)/1000;
     grams = in_metric - kilos*1000;
     cout << " \n";
}

void output(int kilos, double grams)
{
     cout << "Per Metric System, your total mass is: " << kilos <<
     " kilograms and " << fixed << setprecision(2) << grams << " grams \n";
}

void input(double&pounds, double&ounces)
{
    cout << "How many pounds do you weigh, however the largest integer " <<
    "less than or equal to your weight \n";
    cin >> pounds;
    cout << "Now, the Remainder in ounces, decimals okay \n";
    cin >> ounces;
}
    


