/*
* Name: Benjamin Cornjeo
* Student ID: 2203277
* Date September 16 2013
* HW: 2
* Problem: 1 A and B
* I Certify this is my own work and Code.
*/

/*
Logic Error: Console displays product instead of sum;
Syntax Error: no comma between Variable integers a, and b, declaration.
should be: int a, b;
*/

#include <iostream>
using namespace std;
int main()
{
    cout << "Hello \n";
    int a b;
    cout << "This Console will generate a sum for any two numbers. Enter any integer \n";
    cin >> a;
    cout << "Enter the other integer that you wish to add to the first \n";
    cin >> b;
    cout << b << " + " << a << " = " << b * a;
    
    cout << " \n";
    cout << " \n";
    
    
    
    
    system("pause");
    return 0;
}
