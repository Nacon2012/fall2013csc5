/*
* Name: Benjamin Cornejo
* Student ID: 2203277
* Date: October 7, 2013
* HW: 5
* Problem: 4
* I certify this is my own work and code
*/



#include <iostream>
#include <cstdlib>
#include <stdlib.h>
using namespace std;

int program_1(string one, string two)
{
    cout << "This is Rock, Paper, Scissors \n";
    cout << "Enter 'R' for Rock, 'P' for Paper, and 'S' for scissors \n";
    cout << "User 1 goes first, then User 2 \n";


    if (   ((one == "r" || one == "R")    &&  (two == "r" || two == "R")) ||  
    ((one == "s" || one == "S")    &&  (two == "s" || two == "S"))  || 
    ((one == "p" || one == "P")    &&  (two == "p" || two == "P"))    )
    {
         cout << "Nobody Wins \n";
    }
    else if ( one == "P" || one == "p")
    {
         if ( two == "S" || two == "s")
         {
              cout << "User 2 wins: Scissors cut Paper :P \n";
         }
         else if ( two == "R" || two == "r")
         {
              cout << "User 1 wins: Paper covers Rock :P \n";
         }
    }
    else if ( one == "R" || one == "r")
    {
         if ( two == "S" || two == "s")
         {
              cout << "User 1 Wins: Rock Breaks Scissors :P \n";
         }
         else if ( two == "P" || two == "p")
         {
              cout << "User 2 wins: Paper covers Rock :P \n";
         }
    }
    else if ( one == "S" || one == "s")
    {
         if ( two == "P" || two == "p")
         {
              cout << "User 1 wins: Scissors cut Paper :P \n";
         }
         else if ( two == "R" || two == "r")
         {
              cout << "User 2 wins: Rock breaks Scissors :P \n";
         }
    }
    
    
}

int program_2()
{
    string fire;
    cout << "This is Rock, Paper, Scissors \n";
    cout << " \n";
    cout << " \n";
    cout << "Computer Bot 1 vs Computer Bot 2 \n";
    
    

    
         
    string one, two;
    srand(time(0));
    int num_1, num_2;
    num_1 = (rand()+5)%3+1;
    num_2 = (rand()+13)%3+1;
    if (num_1 == 1)
    {
              one = "P";
    }
    else if (num_1 == 2)
    {
         one = "R";
    }
    else if (num_1 == 3)
    {
         one = "S";
    }
    if (num_2 == 1)
    {
               two = "P";
    }
    else if (num_2 == 2)
    {
         two = "R";
    }
    else if (num_2 == 3)
    {
         two = "S";
    }
    
    
    
         
    cout << "BOT 1, GO!!! \n";
    cout << " \n";
    cout << "Bot 1 chose " << one;
    cout << " \n";
    cout << " \n";
    cout << "BOT 2, GO!!! \n";
    cout << " \n";
    cout << "Bot 2 chose " << two;
    cout << " \n";
    cout << " \n";
    
    
    if (   ((one == "r" || one == "R")    &&  (two == "r" || two == "R")) ||  
    ((one == "s" || one == "S")    &&  (two == "s" || two == "S"))  || 
    ((one == "p" || one == "P")    &&  (two == "p" || two == "P"))    )
    {
         cout << "Nobody Wins \n";
         cout << " \n";
    }
    else if ( one == "P" || one == "p")
    {
         if ( two == "S" || two == "s")
         {
              cout << "Bot 2 wins: Scissors cut Paper :P \n";
              cout << " \n";
         }
         else if ( two == "R" || two == "r")
         {
              cout << "Bot 1 wins: Paper covers Rock :P \n";
              cout << " \n";
         }
    }
    else if ( one == "R" || one == "r")
    {
         if ( two == "S" || two == "s")
         {
              cout << "Bot 1 Wins: Rock Breaks Scissors :P \n";
              cout << " \n";
         }
         else if ( two == "P" || two == "p")
         {
              cout << "Bot 2 wins: Paper covers Rock :P \n";
              cout << " \n";
         }
    }
    else if ( one == "S" || one == "s")
    {
         if ( two == "P" || two == "p")
         {
              cout << "Bot 1 wins: Scissors cut Paper :P \n";
              cout << " \n";
         }
         else if ( two == "R" || two == "r")
         {
              cout << "Bot 2 wins: Rock breaks Scissors :P \n";
              cout << " \n";
         }
    }
    
    
    
    
}
    

int main()
{
    cout << "Select a game" << endl;
    cout << "1 for 2 user play" << endl;
    cout << "2 for 2 bot play" << endl;
    int selection;
    cin >> selection;
    switch (selection)
    {
    case 2:
                
    {
                
    string fire;
    string one, two;
    
    cout << " \n";
           
    cout << "This is Rock, Paper, Scissors \n";
    cout << " \n";
    cout << " \n";
    cout << "Computer Bot 1 vs Computer Bot 2 \n";
    do
    {
         cout << "Enter 'Fire' to Commence Super Bot Gore, 'exit' to Leave \n";
         cin >> fire;
         program_2();
         
    }
    while ( fire != "exit");
    
    
           
    break;
    }
    
    case 1: 
    {
    
    string one, two;
    do
    {
           
         cout << "At Any Moment, One user enter, 'E' to exit \n";
         cout << "User 1, GO!!! \n";
         cin >> one;
         cout << " \n";
         cout << "User 2, GO!!! \n";
         cin >> two;
         cout << " \n";
         program_1(one, two);
    }
         while ( one != "E" && two != "E");
    
    


                
           break;
    }
    }
    
    
    cout << " \n";
    system("pause");
    return 0;
}
