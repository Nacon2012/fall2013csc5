/*
* Name: Benjamin Cornejo
* Student ID: 2203277
* Date: September 21, 2013
* HW: 3
* Problem: 6
* I certify this is my own work and code
*/

#include <iostream>
using namespace std;
int main()
{
    int a, x_neg = 0, y_pos = 0, count_neg = 0, count_pos = 0;
    float ave_neg, ave_pos;
    cout << "Won't you Please Enter A Number \n";
    cin >> a;
    if (a < 0)
    {
          x_neg = a;
          count_neg += 1;      
    }
    else
    {
        y_pos = a;
        count_pos += 1;
    }
    for(int i = 0; i < 9; i++)
    {
            cout << "Won't you Please Enter A Number \n";
            cin >> a;
            if (a < 0)
            {
                  x_neg += a;
                  count_neg += 1;
            }
            else
            {
                  y_pos += a;
                  count_pos += 1;
            }
    }
    
    cout << "The Sum of all Positive Numbers is: " << y_pos << " \n";
    cout << "The Sum of all Negative Numbers is: " << x_neg << " \n";
    cout << "The Sum of all Numbers is: " << x_neg + y_pos << " \n";
    ave_pos = static_cast<float>(y_pos)/static_cast<float>(count_pos);
    ave_neg = static_cast<float>(x_neg)/static_cast<float>(count_neg);
    cout << "The Average of all Positive Numbers is: " << ave_pos << " \n";
    cout << "The Average of all Negative Numbers is: " << ave_neg << " \n";
    cout << "The Average of all Numbers is: " << (ave_neg + ave_pos)/10;
    
    
    
    
    cout << " \n";
    system("pause");
    return 0;
}
